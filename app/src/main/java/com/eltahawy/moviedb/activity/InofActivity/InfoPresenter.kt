package com.eltahawy.moviedb.activity.InofActivity

import com.android.volley.Request
//import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.eltahawy.moviedb.data.retrofitsingleton
import com.eltahawy.moviedb.data.volleysingleton
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InfoPresenter(view:InfoContract.view): InfoContract.presenter  {

    val mView:InfoContract.view = view;

    override fun getMovies(id:String) {
        createRequeset(id)
    }

    private fun createRequeset(id:String) {
        try {
            val call = retrofitsingleton.moviesApi.getInfo(id)
            call.enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>?, t: Throwable?) {
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    mView.setMovies(response.body()!!)
                }

            })


        }catch (e:Exception){
            println(e.message)
        }
    }
}