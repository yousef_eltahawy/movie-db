package com.eltahawy.moviedb.activity.MainActivity

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.eltahawy.moviedb.data.Movie
import com.eltahawy.moviedb.data.retrofitsingleton
import com.eltahawy.moviedb.data.volleysingleton
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class MainPresenter(view:MainContract.view):MainContract.presenter {
    val mView:MainContract.view=view
    override fun getMovies(list_name: String,recycle:androidx.recyclerview.widget.RecyclerView) {
        createRequeset(list_name,recycle)

    }

    private fun createRequeset(list_name:String,recycle:androidx.recyclerview.widget.RecyclerView) {
        try {
                val call = retrofitsingleton.moviesApi.getList(list_name, 1)
                call.enqueue(object : Callback<String> {
                    override fun onFailure(call: Call<String>?, t: Throwable?) {}

                    override fun onResponse(
                        call: Call<String>,
                        response: retrofit2.Response<String>
                    ) {
                            getMovirList(response.body()!!,recycle)
                    }
                })
        } catch (e: Exception) {
            println(e.message)
        }


    }
    private fun getMovirList(jsonObj:String,recycle:androidx.recyclerview.widget.RecyclerView){
        var movieList = ArrayList<Movie>()
        try{
            val results = JSONObject(jsonObj).getJSONArray("results")
            for (item in 0 until results.length()) {
                var movie = Movie(null, null, null)
                val id = results.getJSONObject(item).getString("id")
                val poster = results.getJSONObject(item).getString("poster_path")
                val title = results.getJSONObject(item).getString("title")
                movie.id = id
                movie.title = title
                movie.image = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"+poster
                movieList.add(movie)
            }
        }catch(e:Exception){
            println(e.message)
        }
        mView.setMovies(movieList,recycle)
    }
}