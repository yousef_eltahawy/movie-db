package com.eltahawy.moviedb.activity.GridActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.eltahawy.moviedb.adapters.CustomGrid
import com.eltahawy.moviedb.R
import com.eltahawy.moviedb.activity.InofActivity.InfoActivity
import com.eltahawy.moviedb.activity.MainActivity.MainActivity
import com.eltahawy.moviedb.data.Movie
import kotlinx.android.synthetic.main.activity_grid.*


class GridActivity : AppCompatActivity(),GridContract.view {

    val mPresenter:GridContract.presenter=GridPresenter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid)
        mPresenter.getMovies(intent.getStringExtra("list_name"))
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater=menuInflater
            inflater.inflate(R.menu.action_buttons,menu)
            val menuItem=menu!!.findItem(R.id.search)
            val searchview= menuItem.actionView as androidx.appcompat.widget.SearchView
            searchview.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    var queryString =query.replace(" ","%20",true)
                    val intent= Intent(applicationContext,
                        GridActivity::class.java).apply{}
                    intent.putExtra("list_name","https://api.themoviedb.org/3/search/movie?api_key=5d86bb8d52df441511ca1197329e6241&query="
                            +queryString+"&page=")
                    startActivity(intent)
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    return false
                }
            })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.home ->{
                val intent=Intent(applicationContext,
                    MainActivity::class.java).apply {  }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
        }
        return true
    }

    override fun setMovies(list:ArrayList<Movie>){

        grid_view.adapter =
            CustomGrid(applicationContext, list)
        var movie: Movie
        grid_view.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(applicationContext, InfoActivity::class.java).apply {}
            movie = list.get(position)
            intent.putExtra("id", movie.id)
            startActivity(intent)
        }

    }


}
