package com.eltahawy.moviedb.activity.MainActivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.View
import com.eltahawy.moviedb.adapters.CustomRecycle
import com.eltahawy.moviedb.activity.GridActivity.GridActivity
import com.eltahawy.moviedb.R
import com.eltahawy.moviedb.data.Movie
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener, MainContract.view {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val layoutManager1 = androidx.recyclerview.widget.LinearLayoutManager(
            applicationContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        val layoutManager2 = androidx.recyclerview.widget.LinearLayoutManager(
            applicationContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        val layoutManager3 = androidx.recyclerview.widget.LinearLayoutManager(
            applicationContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        val layoutManager4 = androidx.recyclerview.widget.LinearLayoutManager(
            applicationContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        top_rated.layoutManager = layoutManager1
        popular.layoutManager = layoutManager2
        upcoming.layoutManager = layoutManager3
        now_playing.layoutManager = layoutManager4

        val mPresenter: MainContract.presenter = MainPresenter(this)
        mPresenter.getMovies("top_rated", top_rated)
        mPresenter.getMovies("popular", popular)
        mPresenter.getMovies("upcoming", upcoming)
        mPresenter.getMovies("now_playing", now_playing)
        topText.setOnClickListener(this)
        popularText.setOnClickListener(this)
        upText.setOnClickListener(this)
        nowText.setOnClickListener(this)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_item, menu)
        val menuItem = menu!!.findItem(R.id.solosearch)
        val searchview = menuItem.actionView as androidx.appcompat.widget.SearchView
        searchview.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {

                val queryString = query.replace(" ", "%20", true)
                val intent = Intent(
                    applicationContext,
                    GridActivity::class.java
                ).apply {}
                intent.putExtra(
                    "list_name",
                    "https://api.themoviedb.org/3/search/movie?api_key=5d86bb8d52df441511ca1197329e6241&query="
                            + queryString + "&page="
                )
                startActivity(intent)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean { //    adapter.getFilter().filter(newText);
                return false
            }
        })

        return true

    }

    override fun setMovies(
        list: ArrayList<Movie>,
        recycle: androidx.recyclerview.widget.RecyclerView
    ) {
        recycle.adapter =
            CustomRecycle(applicationContext, list)
    }

    override fun onClick(v: View?) {
        var list = ""
        when (v!!.id) {
            R.id.topText -> list = "top_rated"
            R.id.popularText -> list = "popular"
            R.id.upText -> list = "upcoming"
            R.id.nowText -> list = "now_playing"
        }
        val intent = Intent(applicationContext, GridActivity::class.java).apply { }
        intent.putExtra("list_name", list)
        intent.putExtra("flag", "false")
        startActivity(intent)
    }
}
