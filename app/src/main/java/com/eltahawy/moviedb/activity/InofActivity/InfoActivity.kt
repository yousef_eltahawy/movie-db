package com.eltahawy.moviedb.activity.InofActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.eltahawy.moviedb.activity.MainActivity.MainActivity
import com.eltahawy.moviedb.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_info.*
import org.json.JSONObject

class InfoActivity : AppCompatActivity(),InfoContract.view {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val id=intent.getStringExtra("id")
        val mpresenter:InfoContract.presenter=InfoPresenter(this)
        mpresenter.getMovies(id)

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater=menuInflater
        inflater.inflate(R.menu.home_item,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            R.id.solohome ->{
                val intent=Intent(applicationContext,
                    MainActivity::class.java).apply {  }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)

            }
        }
        return true
    }

     override fun setMovies(jsonObj: String){


        try {
            val json =JSONObject(jsonObj)
            title=json.getString("title")+" ("+json.getString("release_date").subSequence(0,4)+")"
            val poster_path = json.getString("backdrop_path")
            originaltitle.text=json.getString("original_title")
            budget.text=json.getString("budget")
            revenue.text=json.getString("revenue")
            overview.text=json.getString("overview" )
            rating.text=json.getString("vote_average")
            votecount.text=json.getString( "vote_count")
            language.text=json.getJSONArray("spoken_languages").getJSONObject(0).getString("name")
            runtime.text=(json.getInt("runtime")/60).toString()+"h "+(json.getInt("runtime")%60).toString()+"min"
            val genre=json.getJSONArray("genres")
            var str=""
            for(item in 0 until  genre.length()) {
                str += genre.getJSONObject(item).getString("name")+" | "
            }
            genres.text=str.subSequence(0,str.length-2)
            Picasso.with(applicationContext).load("https://image.tmdb.org/t/p/w500"+poster_path).into(info_image)

        }
        catch (e:Exception){
            println(e.message)
        }
    }
}
