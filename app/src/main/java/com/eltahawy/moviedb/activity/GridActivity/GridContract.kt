package com.eltahawy.moviedb.activity.GridActivity

import com.eltahawy.moviedb.data.Movie

interface GridContract {
    interface  view{
        fun setMovies(list:ArrayList<Movie>)
    }
    interface presenter{
        fun getMovies(list_name:String)
    }
}