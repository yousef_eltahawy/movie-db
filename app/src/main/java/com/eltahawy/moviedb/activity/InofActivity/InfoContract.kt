package com.eltahawy.moviedb.activity.InofActivity

interface InfoContract {
    interface  view{
        fun setMovies(jsonObj: String)
    }
    interface presenter{
        fun getMovies(id:String)
    }
}