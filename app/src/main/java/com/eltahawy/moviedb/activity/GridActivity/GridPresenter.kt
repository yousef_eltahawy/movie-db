package com.eltahawy.moviedb.activity.GridActivity

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.eltahawy.moviedb.data.Movie
import com.eltahawy.moviedb.data.retrofitsingleton
import com.eltahawy.moviedb.data.volleysingleton
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response as Response1

class GridPresenter(view: GridContract.view) : GridContract.presenter {
    val mView: GridContract.view = view
    var movieList = ArrayList<Movie>()
    override fun getMovies(list_name: String) {
        if(list_name.length>30)
            createSearchRequeset(list_name)
        else
            createRequeset(list_name)
    }
    private fun createRequeset(list: String) {
        try {
            val call = retrofitsingleton.moviesApi.getList(list, 1)
            call.enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>?, t: Throwable?) {}

                override fun onResponse(call: Call<String>, response:retrofit2.Response<String>) {
                    var totalpage = JSONObject(response.body()).getInt("total_pages")
                    if (totalpage > 30)
                        totalpage = 30
                    try {
                        for (item in 1..totalpage) {
                            val call = retrofitsingleton.moviesApi.getList(list, item)
                            call.enqueue(object : Callback<String> {
                                override fun onFailure(call: Call<String>?, t: Throwable?) {}

                                override fun onResponse(
                                    call: Call<String>,
                                    response: retrofit2.Response<String>
                                ) {
                                    getMovirList(response.body()!!)
                                }
                            })
                        }
                    } catch (e: Exception) {
                        println(e.message)
                    }
                }
            })
        } catch (e: Exception) {
            println(e.message)
        }

    }

    private fun createSearchRequeset(str:String) {
        val url = str + "1"
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
            Response.Listener { response ->
                var totalpage = JSONObject(response.toString()).getInt("total_pages")
                if(totalpage>30)
                    totalpage=30
                for(item in 1..totalpage) {
                    val url = str + item.toString()
                    val jsonObjectRequest1 = JsonObjectRequest(Request.Method.GET, url, null,
                        Response.Listener { response ->
                            val inString = (response.toString())
                            getMovirList(inString)
                        },
                        Response.ErrorListener { error -> }
                    )
                    volleysingleton.instance?.addToRequestQueue(jsonObjectRequest1)
                }
            },
            Response.ErrorListener { error -> }
        )

        volleysingleton.instance?.addToRequestQueue(jsonObjectRequest)
    }

    private fun getMovirList(jsonObj: String) {
        var totalpage = JSONObject(jsonObj).getInt("total_pages")
        if (totalpage > 30)
            totalpage = 30
        val results = JSONObject(jsonObj).getJSONArray("results")
        for (item in 0 until results.length()) {
            var movie = Movie(null, null, null)
            val id = results.getJSONObject(item).getString("id")
            val poster = results.getJSONObject(item).getString("poster_path")
            val title = results.getJSONObject(item).getString("title")
            movie.id = id
            movie.title = title
            movie.image = poster
            movieList.add(movie)
        }
        if (movieList.size >= ((totalpage - 1) * 20 + 1)) {
            mView.setMovies(movieList)
        }
    }

}