package com.eltahawy.moviedb.activity.MainActivity

import com.eltahawy.moviedb.data.Movie

interface MainContract {
    interface  view{
        fun setMovies(list:ArrayList<Movie>,recycle:androidx.recyclerview.widget.RecyclerView)
    }
    interface presenter{
        fun getMovies(list_name:String,recycle:androidx.recyclerview.widget.RecyclerView)
    }
}