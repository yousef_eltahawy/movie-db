package com.eltahawy.moviedb.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

object retrofitsingleton {
    val moviesApi :MoviesApi by lazy {

        val retrofit=Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/movie/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
        return@lazy retrofit.create(MoviesApi::class.java)
    }
}