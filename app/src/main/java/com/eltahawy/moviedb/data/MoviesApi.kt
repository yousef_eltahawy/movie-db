package com.eltahawy.moviedb.data

import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApi {
    @GET("{id}?api_key=5d86bb8d52df441511ca1197329e6241")
    fun getInfo(@Path("id") id:String):Call<String>

    @GET("{list}?api_key=5d86bb8d52df441511ca1197329e6241")
    fun getList(@Path("list") list:String,@Query("page") page:Int):Call<String>
}