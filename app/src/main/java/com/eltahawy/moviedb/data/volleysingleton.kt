package com.eltahawy.moviedb.data

import android.app.Application
import android.text.TextUtils
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class volleysingleton:Application() {
    override fun onCreate() {
        super.onCreate()
        instance=this
    }
    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }
    companion object {
        private val TAG = volleysingleton::class.java.simpleName
        @get:Synchronized var instance: volleysingleton? = null
            private set
    }
}