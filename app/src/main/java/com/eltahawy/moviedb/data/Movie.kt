package com.eltahawy.moviedb.data

import android.media.Image

class Movie {
  var image:String?=null
  var id:String?=null
  var title:String?=null
  constructor(image:String?,id:String?,title: String?){
    this.image=image
    this.id=id
    this.title=title
  }
}
