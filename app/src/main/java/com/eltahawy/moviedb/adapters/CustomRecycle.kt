package com.eltahawy.moviedb.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.eltahawy.moviedb.R
import com.eltahawy.moviedb.activity.InofActivity.InfoActivity
import com.eltahawy.moviedb.data.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_grid_item.view.*

class CustomRecycle(val context:Context, val movieList:ArrayList<Movie>):
    androidx.recyclerview.widget.RecyclerView.Adapter<CustomRecycle.movieHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): movieHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.movie_grid_item,parent,false)
        return movieHolder(view)
    }

    override fun onBindViewHolder(holder: movieHolder, position: Int) {
        val currentItem=movieList.get(position)
        Picasso.with(context).load(currentItem.image).into(holder.image)
        holder.title.text=currentItem.title
        holder.itemView.setOnClickListener {
            val intent = Intent(context, InfoActivity::class.java).apply {}
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("id", currentItem.id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount()=movieList.size


    class movieHolder(view:View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        var image:ImageView=view.grid_image
        var title:TextView=view.grid_text

    }
}