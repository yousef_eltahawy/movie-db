package com.eltahawy.moviedb.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.eltahawy.moviedb.R
import com.eltahawy.moviedb.data.Movie
import com.squareup.picasso.Picasso

class CustomGrid(val context:Context,val arrayList:ArrayList<Movie>):BaseAdapter() {

    override fun getItem(position: Int): Any {
        return arrayList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return arrayList.size
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(context,
            R.layout.movie_grid_item, null)
        var poster: ImageView = view.findViewById(R.id.grid_image)
        var title:TextView=view.findViewById(R.id.grid_text)
        val movie: Movie = arrayList.get(position)
        title.text=movie.title
        Picasso.with(context).load("https://image.tmdb.org/t/p/w600_and_h900_bestv2"+movie.image).into(poster)
        return view
    }
}
